'use strict';

// ready
$(document).ready(function() {

    // slider
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            767:{
                items:3
            },
            1000:{
                items:5,
                loop:false
            }
        }
    });
    $('.main-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        navText:["",""],
        items:1
    });

    $('.reviewBlock-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:false,
        navText:["<i class='icon-left-open'></i>","<i class='icon-right-open'></i>"],
        items:1,
        responsive:{
            0:{
                nav:false
            },
            768:{
                nav:true
            }
        }
    });

    $('.content-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:true,
        navText:["<i class='icon-left-dir'></i>","<i class='icon-right-dir'></i>"],
        items:1,
        responsive:{
            0:{
                nav:false
            },
            768:{
                nav:true
            }
        }
    });
    // slider


    $('.searchItem').click(function(){
        $(this).addClass('active');
        return false;
    });

    $('.subitem').click(function(){
        $(this).toggleClass('active');
        $(this).find('.subnav').slideToggle();
        return false;
    });


    $('.feedbackFix').on('click', '.dropdown-menu', function(e) {
        e.stopPropagation();
    });

     //mask type = phone
     //$("[name=phone]").mask("+7 (999) 999-9999");


    $(function($) {
        var allAccordions = $('.accordion div.data');
        var allAccordionItems = $('.accordion .accordion-item');
        $('.accordion > .accordion-item').click(function() {
            if($(this).hasClass('open'))
            {
                $(this).removeClass('open');
                $(this).next().slideUp("slow");
            }
            else
            {
                allAccordions.slideUp("slow");
                allAccordionItems.removeClass('open');
                $(this).addClass('open');
                $(this).next().slideDown("slow");
                return false;
            }
        });
    });


    $(function(){
        var s = $(".data tr");
        s.each(function(indx, el){
            $(":checkbox", el).click(function(event) {
                $(el)[($(el).has(":checked").length ? "add" : "remove")+"Class"]("active");
            });
        });
    });

    function close_accordion_section() {
        $('.accordion .accordion-section-title').removeClass('active');
        $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    $('.accordion-section-title').click(function(e) {
        var currentAttrValue = $(this).attr('href');
        if($(e.target).is('.active')) {
            close_accordion_section();
        }else {
            close_accordion_section();
            $(this).addClass('active');
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }
        e.preventDefault();
    });

    $(".fancybox").fancybox();

});
// ready

// load
//$(document).load(function() {});
// load

// scroll
//$(window).scroll(function() { });
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 768) {}
// mobile sctipts